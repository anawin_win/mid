package com.anawin.mid;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Store alcohol = new Store("alcoholic beverages", 20, 2000);
        Store snack = new Store("snack", 100, 100);
        Store cigarette = new Store("cigarette", 10, 500);
        Store drink = new Store("drink", 100, 150);
        PizzaKub margherita = new PizzaKub("margherita", 50, 1500);
        PizzaKub hawaiian = new PizzaKub("Hawaiian", 50, 1500);
        PizzaKub vegetarian = new PizzaKub("Vegetarian", 50, 1300);
        PizzaKub anchovyandolives = new PizzaKub("Anchovy and Olives ", 50, 1300);
        PizzaKub meatfeast = new PizzaKub("Meat Feast", 50, 1800);
        PizzaKub pepperoni = new PizzaKub("Pepperoni", 50, 1300);
        PizzaKub bbqchicken = new PizzaKub("BBQ Chicken", 50, 1400);
        PizzaKub smokedsalmon = new PizzaKub("SmokedSalmon", 50, 1700);
        PizzaKub nutellapizza = new PizzaKub("NutellaPizza", 50, 1000);
        while (true) {
            System.out.println("============Enter Choice============");
            System.out.println("1.Store");
            System.out.println("2.PizzaKub");
            System.out.println("3.Exit");
            System.out.println("====================================");
            Scanner sc = new Scanner(System.in);
            int ch = sc.nextInt();
            if (ch == 1) {
                int num = 0;
                System.out.println("==========Welcome to Store==========");
                while (true) {

                    System.out.println("===========Please Select============");
                    System.out.println("1.show product list");
                    System.out.println("2.select product list");
                    System.out.println("3.show expenses");
                    System.out.println("4.make payment");
                    System.out.println("0.Exit");
                    System.out.println("====================================");
                    num = sc.nextInt();
                    if (num == 1) {
                        System.out.println("===============STOCK================");
                        alcohol.print();
                        cigarette.print();
                        snack.print();
                        drink.print();
                        System.out.println("====================================");
                    } else if (num == 2) {
                        System.out.println("============PRODUCT LIST============");
                        System.out.println("1." + alcohol.getItemname());
                        System.out.println("2." + cigarette.getItemname());
                        System.out.println("3." + snack.getItemname());
                        System.out.println("4." + drink.getItemname());
                        System.out.println("====================================");
                        int num1 = sc.nextInt();
                        while (true) {
                            if (num1 == 1) {
                                System.out.print("want to receive " + alcohol.getItemname() + " : ");
                                int n = sc.nextInt();
                                alcohol.itemselect(n);
                                break;
                            } else if (num1 == 2) {
                                System.out.print("want to receive " + cigarette.getItemname() + " : ");
                                int n = sc.nextInt();
                                cigarette.itemselect(n);
                                break;
                            } else if (num1 == 3) {
                                System.out.print("want to receive " + snack.getItemname() + " : ");
                                int n = sc.nextInt();
                                snack.itemselect(n);
                                break;
                            } else if (num1 == 4) {
                                System.out.print("want to receive " + drink.getItemname() + " : ");
                                int n = sc.nextInt();
                                drink.itemselect(n);
                                break;
                            } else {
                                System.out.println("no option");
                                System.out.println("Please do the transaction again.;(");
                                break;
                            }
                        }
                    } else if (num == 3) {
                        System.out.println("=============TOTAL LIST=============");
                        Double total = alcohol.calculate() + cigarette.calculate() + snack.calculate()
                                + drink.calculate();
                        alcohol.printTotal();
                        cigarette.printTotal();
                        snack.printTotal();
                        drink.printTotal();
                        System.out.println("TOTAl                        " + total);
                        System.out.println("====================================");
                    } else if (num == 4) {
                        Double total = alcohol.calculate() + cigarette.calculate() + snack.calculate()
                                + drink.calculate();
                        System.out.println("============MAKE PAYMENT============");
                        System.out.print("please enter cash : ");
                        double cash = sc.nextDouble();
                        System.out.println("product cost : " + total);
                        if (cash < total) {
                            System.out.println("your money is not enough");
                            System.out.println("Please do the transaction again.;(");
                            break;
                        }
                        Double change = cash - total;
                        System.out.println("change : " + change);
                        System.out.println("====================================");
                    } else if (num == 0) {
                        System.out.println("Thank you for using the service.:)");
                        break;
                        
                    }
                }
            } else if (ch == 2) {
                int num = 0;
                System.out.println("=======(>Welcome to PizzaKub<)======");
                while (true) {
                    System.out.println("===========Please Select============");
                    System.out.println("1.show menu list");
                    System.out.println("2.select menu list");
                    System.out.println("3.show expenses");
                    System.out.println("4.make payment");
                    System.out.println("0.Exit");
                    System.out.println("====================================");
                    num = sc.nextInt();
                    if (num == 1) {
                        System.out.println("================MENU================");
                        margherita.print();
                        hawaiian.print();
                        vegetarian.print();
                        anchovyandolives.print();
                        pepperoni.print();
                        bbqchicken.print();
                        smokedsalmon.print();
                        nutellapizza.print();
                        System.out.println("====================================");
                    } else if (num == 2) {
                        System.out.println("==============MENU LIST=============");
                        System.out.println("1." + margherita.getmenuname());
                        System.out.println("2." + hawaiian.getmenuname());
                        System.out.println("3." + vegetarian.getmenuname());
                        System.out.println("4." + anchovyandolives.getmenuname());
                        System.out.println("5." + meatfeast.getmenuname());
                        System.out.println("6." + pepperoni.getmenuname());
                        System.out.println("7." + bbqchicken.getmenuname());
                        System.out.println("8." + smokedsalmon.getmenuname());
                        System.out.println("9." + nutellapizza.getmenuname());
                        System.out.println("====================================");
                        int num1 = sc.nextInt();
                        while (true) {
                            if (num1 == 1) {
                                System.out.print("want to receive " + margherita.getmenuname() + " : ");
                                int n = sc.nextInt();
                                margherita.itemselect(n);
                                break;
                            } else if (num1 == 2) {
                                System.out.print("want to receive " + hawaiian.getmenuname() + " : ");
                                int n = sc.nextInt();
                                hawaiian.itemselect(n);
                                break;
                            } else if (num1 == 3) {
                                System.out.print("want to receive " + vegetarian.getmenuname() + " : ");
                                int n = sc.nextInt();
                                vegetarian.itemselect(n);
                                break;
                            } else if (num1 == 4) {
                                System.out.print("want to receive " + anchovyandolives.getmenuname() + " : ");
                                int n = sc.nextInt();
                                anchovyandolives.itemselect(n);
                                break;
                            } else if (num1 == 5) {
                                System.out.print("want to receive " + meatfeast.getmenuname() + " : ");
                                int n = sc.nextInt();
                                pepperoni.itemselect(n);
                                break;
                            } else if (num1 == 6) {
                                System.out.print("want to receive " + pepperoni.getmenuname() + " : ");
                                int n = sc.nextInt();
                                pepperoni.itemselect(n);
                                break;
                            } else if (num1 == 7) {
                                System.out.print("want to receive " + bbqchicken.getmenuname() + " : ");
                                int n = sc.nextInt();
                                bbqchicken.itemselect(n);
                                break;
                            } else if (num1 == 8) {
                                System.out.print("want to receive " + smokedsalmon.getmenuname() + " : ");
                                int n = sc.nextInt();
                                smokedsalmon.itemselect(n);
                                break;
                            } else if (num1 == 9) {
                                System.out.print("want to receive " + nutellapizza.getmenuname() + " : ");
                                int n = sc.nextInt();
                                nutellapizza.itemselect(n);
                                break;
                            } else {
                                System.out.println("no option");
                                System.out.println("Please do the transaction again.;(");
                                break;
                            }
                        }
                    } else if (num == 3) {
                        System.out.println("=============TOTAL LIST=============");
                        Double total = margherita.calculate() + hawaiian.calculate() + vegetarian.calculate()
                                + anchovyandolives.calculate() + meatfeast.calculate() + pepperoni.calculate()
                                + bbqchicken.calculate() + smokedsalmon.calculate() + nutellapizza.calculate();
                        margherita.printTotal();
                        hawaiian.printTotal();
                        vegetarian.printTotal();
                        anchovyandolives.printTotal();
                        meatfeast.printTotal();
                        pepperoni.printTotal();
                        bbqchicken.printTotal();
                        smokedsalmon.printTotal();
                        nutellapizza.printTotal();
                        System.out.println("TOTAl                        " + total);
                        System.out.println("====================================");
                    } else if (num == 4) {
                        Double total = margherita.calculate() + hawaiian.calculate() + vegetarian.calculate()
                                + anchovyandolives.calculate() + meatfeast.calculate() + pepperoni.calculate()
                                + bbqchicken.calculate() + smokedsalmon.calculate() + nutellapizza.calculate();
                        System.out.println("============MAKE PAYMENT============");
                        System.out.print("please enter cash : ");
                        double cash = sc.nextDouble();
                        System.out.println("product cost : " + total);
                        if (cash < total) {
                            System.out.println("your money is not enough");
                            System.out.println("Please do the transaction again.;(");
                            break;
                        }
                        Double change = cash - total;
                        System.out.println("change : " + change);
                        System.out.println("====================================");
                    } else if (num == 0) {
                        System.out.println("Thank you for using the service.:)");
                        break;
                    }
                }
            } else if (ch == 3) {
                System.out.println("Thank you for using the service.:)");
                System.exit(0);
            } else {
                System.out.println("Please do the transaction again.;(");
            }

        }
    }
}

package com.anawin.mid;

public class Store {
    private String itemname;
    private double stock;
    private double price;
    private double purchase = 0;
    public Store(String itemname,double stock,double price){
        this.itemname = itemname;
        this.stock = stock;
        this.price = price;
        this.purchase = purchase;
    }
    public void print(){
        System.out.println(itemname+" have "+stock+" in stock "+"price "+price);
    }
    public String getItemname(){
        return itemname;
    }
    public double getStock(){
        return stock;
    }
    public double getPrice(){
        return price;
    }
    public double getPurchase(){
        return purchase;
    }
    public boolean itemselect(int n){
        if(n > stock){
            System.out.println("out of stock");
            return false;
        };
        stock = stock-n;
        purchase = purchase+n;
        System.out.println(itemname+" : "+n);
        
        return true;
    }
    public double calculate(){
        double total = 0;
        total = purchase*price;
        return total;
    }
    public void printTotal(){
        System.out.println(purchase+"  "+itemname+"              "+calculate());
    }

    
}


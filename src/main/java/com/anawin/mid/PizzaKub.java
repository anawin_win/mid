package com.anawin.mid;

public class PizzaKub {
    private String menuname;
    private double stock;
    private double price;
    private double purchase = 0;
    public PizzaKub(String menuname,double stock,double price){
        this.menuname = menuname;
        this.stock = stock;
        this.price = price;
        this.purchase = purchase;
    }
    public void print(){
        System.out.println(menuname+" have "+stock+" in stock "+"price "+price);
    }
    public String getmenuname(){
        return menuname;
    }
    public double getStock(){
        return stock;
    }
    public double getPrice(){
        return price;
    }
    public double getPurchase(){
        return purchase;
    }
    public boolean itemselect(int n){
        if(n > stock){
            System.out.println("out of stock");
            return false;
        };
        stock = stock-n;
        purchase = purchase+n;
        System.out.println(menuname+" : "+n);
        
        return true;
    }
    public double calculate(){
        double total = 0;
        total = purchase*price;
        return total;
    }
    public void printTotal(){
        System.out.println(purchase+"  "+menuname+"     "+calculate());
    }

    
}

